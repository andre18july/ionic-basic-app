import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public resultado: String = "Resultado";
  public precoAlcool = null;
  public precoGasolina = null;

  calcular(){
    if(this.precoAlcool && this.precoGasolina){
      let pAlcool = parseFloat(this.precoAlcool);
      let pGasolina = parseFloat(this.precoGasolina);

      let resultado = pAlcool / pGasolina;
      if(resultado >= 0.7){
        this.resultado = "Melhor utilizar gasolina";
      }else{
        this.resultado = "Melhor utilizar alcool";
      }
    }else{
      this.resultado = "não preenchidos";
    }
  }

}
